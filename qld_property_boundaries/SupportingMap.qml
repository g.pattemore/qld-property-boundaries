<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis simplifyMaxScale="1" styleCategories="AllStyleCategories" minScale="1e+08" labelsEnabled="1" version="3.10.5-A Coruña" readOnly="0" simplifyDrawingHints="1" maxScale="0" simplifyAlgorithm="0" hasScaleBasedVisibilityFlag="0" simplifyLocal="1" simplifyDrawingTol="1">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 symbollevels="1" enableorderby="0" type="categorizedSymbol" forceraster="0" attr="IF(&quot;S20AH&quot; IS NULL, &quot;VM_STATUS&quot;, CONCAT(&quot;VM_STATUS&quot;,&quot;S20AH&quot;))&#xa;">
    <categories>
      <category symbol="0" label="Category A or B area containing endangered" value="rem_end" render="true"/>
      <category symbol="1" label="Category A or B area containing of concern" value="rem_oc" render="true"/>
      <category symbol="2" label="Category A or B area that is least concern" value="rem_leastc" render="true"/>
      <category symbol="3" label="Category A or B area containing endangered and is S20AH" value="rem_endVMA S20AH area" render="true"/>
      <category symbol="4" label="Category A or B area containing of concern and is S20AH" value="rem_ocVMA S20AH area" render="true"/>
      <category symbol="5" label="Category A or B area that is least concern and is S20AH" value="rem_leastcVMA S20AH area" render="true"/>
      <category symbol="6" label="Category C or R area containing endangered" value="hvr_end" render="true"/>
      <category symbol="7" label="Category C or R area containing of concern" value="hvr_oc" render="true"/>
      <category symbol="8" label="Category C or R area that is of least concern" value="hvr_leastc" render="true"/>
      <category symbol="9" label="Water (S20AH)" value="water VMA S20AH area" render="true"/>
      <category symbol="10" label="Water" value="water" render="true"/>
      <category symbol="11" label="Category X area" value="non_remnant" render="true"/>
      <category symbol="12" label="Not defined or map rendering error" value="" render="false"/>
    </categories>
    <symbols>
      <symbol clip_to_extent="1" name="0" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,54,255,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="101,101,101,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="1"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="1" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,101,15,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="101,101,101,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="1"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="10" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="225,255,255,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="11" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="0" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="254,255,255,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="2,2,2,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="12" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="0" class="ShapeburstFill" locked="0">
          <prop k="blur_radius" v="0"/>
          <prop k="color" v="50,6,54,255"/>
          <prop k="color1" v="0,0,255,255"/>
          <prop k="color2" v="0,255,0,255"/>
          <prop k="color_type" v="0"/>
          <prop k="discrete" v="0"/>
          <prop k="distance_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="distance_unit" v="MM"/>
          <prop k="gradient_color2" v="255,255,255,255"/>
          <prop k="ignore_rings" v="0"/>
          <prop k="max_distance" v="5"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="rampType" v="gradient"/>
          <prop k="use_whole_shape" v="1"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="2" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="125,255,177,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="101,101,101,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="1"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="3" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,54,255,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="255,255,115,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="1.7"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="4" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,101,15,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="255,255,115,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="1.7"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="5" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="125,255,177,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="255,255,115,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="1.7"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="6" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,168,255,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="168,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="1"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="7" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="255,208,145,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="168,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="1"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="8" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="215,255,177,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="168,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="1"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
      <symbol clip_to_extent="1" name="9" force_rhr="0" type="fill" alpha="1">
        <layer enabled="1" pass="1" class="SimpleFill" locked="0">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="225,255,255,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="0,0,0,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0"/>
          <prop k="outline_width_unit" v="Point"/>
          <prop k="style" v="solid"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" value="" type="QString"/>
              <Option name="properties"/>
              <Option name="type" value="collection" type="QString"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style previewBkgrdColor="255,255,255,255" blendMode="0" isExpression="1" fontStrikeout="0" useSubstitutions="0" fontWordSpacing="0" fieldName="CONCAT(&quot;RE_LABEL&quot;, '\n', &quot;PC_LABEL&quot; )" textColor="0,0,0,255" fontLetterSpacing="0" multilineHeight="1" namedStyle="Normal" textOpacity="1" fontUnderline="0" textOrientation="horizontal" fontSize="6" fontCapitals="0" fontSizeUnit="Point" fontKerning="1" fontFamily="Sans Serif" fontItalic="0" fontWeight="50" fontSizeMapUnitScale="3x:0,0,0,0,0,0">
        <text-buffer bufferDraw="1" bufferSize="0.7" bufferBlendMode="0" bufferOpacity="1" bufferNoFill="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferJoinStyle="128" bufferSizeUnits="MM" bufferColor="255,255,255,255"/>
        <background shapeBorderWidth="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeJoinStyle="64" shapeRotation="0" shapeSizeY="0" shapeRadiiY="0" shapeSVGFile="" shapeSizeUnit="MM" shapeSizeType="0" shapeBorderColor="128,128,128,255" shapeOffsetX="0" shapeRotationType="0" shapeRadiiUnit="MM" shapeFillColor="255,255,255,255" shapeBlendMode="0" shapeRadiiX="0" shapeOffsetUnit="MM" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeBorderWidthUnit="MM" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeOffsetY="0" shapeType="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeOpacity="1">
          <symbol clip_to_extent="1" name="markerSymbol" force_rhr="0" type="marker" alpha="1">
            <layer enabled="1" pass="0" class="SimpleMarker" locked="0">
              <prop k="angle" v="0"/>
              <prop k="color" v="145,82,45,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="circle"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" value="" type="QString"/>
                  <Option name="properties"/>
                  <Option name="type" value="collection" type="QString"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowColor="0,0,0,255" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetGlobal="1" shadowRadius="1.5" shadowScale="100" shadowUnder="0" shadowOffsetUnit="MM" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetAngle="135" shadowOffsetDist="1" shadowDraw="0" shadowRadiusUnit="MM" shadowRadiusAlphaOnly="0" shadowOpacity="0.7" shadowBlendMode="6"/>
        <dd_properties>
          <Option type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format reverseDirectionSymbol="0" formatNumbers="0" autoWrapLength="0" leftDirectionSymbol="&lt;" placeDirectionSymbol="0" addDirectionSymbol="0" multilineAlign="1" plussign="0" wrapChar="" useMaxLineLengthForAutoWrap="1" rightDirectionSymbol=">" decimals="3"/>
      <placement distMapUnitScale="3x:0,0,0,0,0,0" offsetType="0" preserveRotation="1" repeatDistance="0" placement="0" overrunDistanceUnit="MM" fitInPolygonOnly="0" placementFlags="10" layerType="PolygonGeometry" maxCurvedCharAngleOut="-25" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" geometryGeneratorType="PointGeometry" quadOffset="4" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" maxCurvedCharAngleIn="25" distUnits="MM" centroidInside="1" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" rotationAngle="0" xOffset="0" repeatDistanceUnits="MM" geometryGeneratorEnabled="0" geometryGenerator="" dist="0" yOffset="0" offsetUnits="MM" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" centroidWhole="0" priority="10" overrunDistance="0"/>
      <rendering mergeLines="0" fontMinPixelSize="3" minFeatureSize="1" scaleVisibility="0" limitNumLabels="0" scaleMin="0" zIndex="0" displayAll="0" drawLabels="1" obstacleFactor="2" obstacle="0" upsidedownLabels="0" fontLimitPixelSize="0" labelPerPart="0" scaleMax="0" obstacleType="1" maxNumLabels="2000" fontMaxPixelSize="10000"/>
      <dd_properties>
        <Option type="Map">
          <Option name="name" value="" type="QString"/>
          <Option name="properties"/>
          <Option name="type" value="collection" type="QString"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option name="anchorPoint" value="pole_of_inaccessibility" type="QString"/>
          <Option name="ddProperties" type="Map">
            <Option name="name" value="" type="QString"/>
            <Option name="properties"/>
            <Option name="type" value="collection" type="QString"/>
          </Option>
          <Option name="drawToAllParts" value="false" type="bool"/>
          <Option name="enabled" value="0" type="QString"/>
          <Option name="lineSymbol" value="&lt;symbol clip_to_extent=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; type=&quot;line&quot; alpha=&quot;1&quot;>&lt;layer enabled=&quot;1&quot; pass=&quot;0&quot; class=&quot;SimpleLine&quot; locked=&quot;0&quot;>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option name=&quot;name&quot; value=&quot;&quot; type=&quot;QString&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option name=&quot;type&quot; value=&quot;collection&quot; type=&quot;QString&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString"/>
          <Option name="minLength" value="0" type="double"/>
          <Option name="minLengthMapUnitScale" value="3x:0,0,0,0,0,0" type="QString"/>
          <Option name="minLengthUnit" value="MM" type="QString"/>
          <Option name="offsetFromAnchor" value="0" type="double"/>
          <Option name="offsetFromAnchorMapUnitScale" value="3x:0,0,0,0,0,0" type="QString"/>
          <Option name="offsetFromAnchorUnit" value="MM" type="QString"/>
          <Option name="offsetFromLabel" value="0" type="double"/>
          <Option name="offsetFromLabelMapUnitScale" value="3x:0,0,0,0,0,0" type="QString"/>
          <Option name="offsetFromLabelUnit" value="MM" type="QString"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property key="dualview/previewExpressions" value="VM_STAT_ID"/>
    <property key="embeddedWidgets/0/id" value="transparency"/>
    <property key="embeddedWidgets/count" value="1"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory maxScaleDenominator="1e+08" penColor="#000000" scaleBasedVisibility="0" opacity="1" scaleDependency="Area" backgroundColor="#ffffff" backgroundAlpha="255" minimumSize="0" lineSizeType="MM" barWidth="5" width="15" sizeScale="3x:0,0,0,0,0,0" minScaleDenominator="0" rotationOffset="270" sizeType="MM" diagramOrientation="Up" penAlpha="255" height="15" penWidth="0" lineSizeScale="3x:0,0,0,0,0,0" labelPlacementMethod="XHeight" enabled="0">
      <fontProperties description="Ubuntu,11,-1,5,50,0,0,0,0,0" style=""/>
      <attribute label="" field="" color="#000000"/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings placement="1" obstacle="0" linePlacementFlags="18" priority="0" zIndex="0" dist="0" showAll="1">
    <properties>
      <Option type="Map">
        <Option name="name" value="" type="QString"/>
        <Option name="properties"/>
        <Option name="type" value="collection" type="QString"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions geometryPrecision="0" removeDuplicateNodes="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option name="QgsGeometryGapCheck" type="Map">
        <Option name="allowedGapsBuffer" value="0" type="double"/>
        <Option name="allowedGapsEnabled" value="false" type="bool"/>
        <Option name="allowedGapsLayer" value="" type="QString"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <fieldConfiguration>
    <field name="RE">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="RE1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="RE2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="RE3">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="RE4">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="RE5">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="PERCENT">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="PC1">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="PC2">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="PC3">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="PC4">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="PC5">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="RE_LABEL">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="PC_LABEL">
      <editWidget type="TextEdit">
        <config>
          <Option type="Map">
            <Option name="IsMultiline" value="false" type="bool"/>
            <Option name="UseHtml" value="false" type="bool"/>
          </Option>
        </config>
      </editWidget>
    </field>
    <field name="LANDZONE">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="BD_STATUS">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="BD_SYMBOL">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="VM_CLASS">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="VM_SYMBOL">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="VM_POLY">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="VERSION">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="L">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="V">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="BVG1M">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="BVG1M_PC">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="DBVG1M">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="DBVG2M">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="DBVG5M">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="S20AH">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="S20AI">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="S20AM">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="SOURCE">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="VM_STATUS">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="VM_STAT_ID">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="SCALE">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="OBJECTID">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="MAP_NO">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="layer">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="path">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="RE" name="" index="0"/>
    <alias field="RE1" name="" index="1"/>
    <alias field="RE2" name="" index="2"/>
    <alias field="RE3" name="" index="3"/>
    <alias field="RE4" name="" index="4"/>
    <alias field="RE5" name="" index="5"/>
    <alias field="PERCENT" name="" index="6"/>
    <alias field="PC1" name="" index="7"/>
    <alias field="PC2" name="" index="8"/>
    <alias field="PC3" name="" index="9"/>
    <alias field="PC4" name="" index="10"/>
    <alias field="PC5" name="" index="11"/>
    <alias field="RE_LABEL" name="" index="12"/>
    <alias field="PC_LABEL" name="" index="13"/>
    <alias field="LANDZONE" name="" index="14"/>
    <alias field="BD_STATUS" name="" index="15"/>
    <alias field="BD_SYMBOL" name="" index="16"/>
    <alias field="VM_CLASS" name="" index="17"/>
    <alias field="VM_SYMBOL" name="" index="18"/>
    <alias field="VM_POLY" name="" index="19"/>
    <alias field="VERSION" name="" index="20"/>
    <alias field="L" name="" index="21"/>
    <alias field="V" name="" index="22"/>
    <alias field="BVG1M" name="" index="23"/>
    <alias field="BVG1M_PC" name="" index="24"/>
    <alias field="DBVG1M" name="" index="25"/>
    <alias field="DBVG2M" name="" index="26"/>
    <alias field="DBVG5M" name="" index="27"/>
    <alias field="S20AH" name="" index="28"/>
    <alias field="S20AI" name="" index="29"/>
    <alias field="S20AM" name="" index="30"/>
    <alias field="SOURCE" name="" index="31"/>
    <alias field="VM_STATUS" name="" index="32"/>
    <alias field="VM_STAT_ID" name="" index="33"/>
    <alias field="SCALE" name="" index="34"/>
    <alias field="OBJECTID" name="" index="35"/>
    <alias field="MAP_NO" name="" index="36"/>
    <alias field="layer" name="" index="37"/>
    <alias field="path" name="" index="38"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" field="RE" applyOnUpdate="0"/>
    <default expression="" field="RE1" applyOnUpdate="0"/>
    <default expression="" field="RE2" applyOnUpdate="0"/>
    <default expression="" field="RE3" applyOnUpdate="0"/>
    <default expression="" field="RE4" applyOnUpdate="0"/>
    <default expression="" field="RE5" applyOnUpdate="0"/>
    <default expression="" field="PERCENT" applyOnUpdate="0"/>
    <default expression="" field="PC1" applyOnUpdate="0"/>
    <default expression="" field="PC2" applyOnUpdate="0"/>
    <default expression="" field="PC3" applyOnUpdate="0"/>
    <default expression="" field="PC4" applyOnUpdate="0"/>
    <default expression="" field="PC5" applyOnUpdate="0"/>
    <default expression="" field="RE_LABEL" applyOnUpdate="0"/>
    <default expression="" field="PC_LABEL" applyOnUpdate="0"/>
    <default expression="" field="LANDZONE" applyOnUpdate="0"/>
    <default expression="" field="BD_STATUS" applyOnUpdate="0"/>
    <default expression="" field="BD_SYMBOL" applyOnUpdate="0"/>
    <default expression="" field="VM_CLASS" applyOnUpdate="0"/>
    <default expression="" field="VM_SYMBOL" applyOnUpdate="0"/>
    <default expression="" field="VM_POLY" applyOnUpdate="0"/>
    <default expression="" field="VERSION" applyOnUpdate="0"/>
    <default expression="" field="L" applyOnUpdate="0"/>
    <default expression="" field="V" applyOnUpdate="0"/>
    <default expression="" field="BVG1M" applyOnUpdate="0"/>
    <default expression="" field="BVG1M_PC" applyOnUpdate="0"/>
    <default expression="" field="DBVG1M" applyOnUpdate="0"/>
    <default expression="" field="DBVG2M" applyOnUpdate="0"/>
    <default expression="" field="DBVG5M" applyOnUpdate="0"/>
    <default expression="" field="S20AH" applyOnUpdate="0"/>
    <default expression="" field="S20AI" applyOnUpdate="0"/>
    <default expression="" field="S20AM" applyOnUpdate="0"/>
    <default expression="" field="SOURCE" applyOnUpdate="0"/>
    <default expression="" field="VM_STATUS" applyOnUpdate="0"/>
    <default expression="" field="VM_STAT_ID" applyOnUpdate="0"/>
    <default expression="" field="SCALE" applyOnUpdate="0"/>
    <default expression="" field="OBJECTID" applyOnUpdate="0"/>
    <default expression="" field="MAP_NO" applyOnUpdate="0"/>
    <default expression="" field="layer" applyOnUpdate="0"/>
    <default expression="" field="path" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" field="RE" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="RE1" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="RE2" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="RE3" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="RE4" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="RE5" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="PERCENT" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="PC1" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="PC2" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="PC3" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="PC4" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="PC5" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="RE_LABEL" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="PC_LABEL" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="LANDZONE" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="BD_STATUS" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="BD_SYMBOL" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="VM_CLASS" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="VM_SYMBOL" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="VM_POLY" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="VERSION" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="L" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="V" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="BVG1M" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="BVG1M_PC" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="DBVG1M" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="DBVG2M" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="DBVG5M" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="S20AH" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="S20AI" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="S20AM" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="SOURCE" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="VM_STATUS" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="VM_STAT_ID" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="SCALE" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="1" field="OBJECTID" exp_strength="0" constraints="3" notnull_strength="1"/>
    <constraint unique_strength="0" field="MAP_NO" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="layer" exp_strength="0" constraints="0" notnull_strength="0"/>
    <constraint unique_strength="0" field="path" exp_strength="0" constraints="0" notnull_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="RE" desc="" exp=""/>
    <constraint field="RE1" desc="" exp=""/>
    <constraint field="RE2" desc="" exp=""/>
    <constraint field="RE3" desc="" exp=""/>
    <constraint field="RE4" desc="" exp=""/>
    <constraint field="RE5" desc="" exp=""/>
    <constraint field="PERCENT" desc="" exp=""/>
    <constraint field="PC1" desc="" exp=""/>
    <constraint field="PC2" desc="" exp=""/>
    <constraint field="PC3" desc="" exp=""/>
    <constraint field="PC4" desc="" exp=""/>
    <constraint field="PC5" desc="" exp=""/>
    <constraint field="RE_LABEL" desc="" exp=""/>
    <constraint field="PC_LABEL" desc="" exp=""/>
    <constraint field="LANDZONE" desc="" exp=""/>
    <constraint field="BD_STATUS" desc="" exp=""/>
    <constraint field="BD_SYMBOL" desc="" exp=""/>
    <constraint field="VM_CLASS" desc="" exp=""/>
    <constraint field="VM_SYMBOL" desc="" exp=""/>
    <constraint field="VM_POLY" desc="" exp=""/>
    <constraint field="VERSION" desc="" exp=""/>
    <constraint field="L" desc="" exp=""/>
    <constraint field="V" desc="" exp=""/>
    <constraint field="BVG1M" desc="" exp=""/>
    <constraint field="BVG1M_PC" desc="" exp=""/>
    <constraint field="DBVG1M" desc="" exp=""/>
    <constraint field="DBVG2M" desc="" exp=""/>
    <constraint field="DBVG5M" desc="" exp=""/>
    <constraint field="S20AH" desc="" exp=""/>
    <constraint field="S20AI" desc="" exp=""/>
    <constraint field="S20AM" desc="" exp=""/>
    <constraint field="SOURCE" desc="" exp=""/>
    <constraint field="VM_STATUS" desc="" exp=""/>
    <constraint field="VM_STAT_ID" desc="" exp=""/>
    <constraint field="SCALE" desc="" exp=""/>
    <constraint field="OBJECTID" desc="" exp=""/>
    <constraint field="MAP_NO" desc="" exp=""/>
    <constraint field="layer" desc="" exp=""/>
    <constraint field="path" desc="" exp=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns>
      <column hidden="0" width="-1" name="RE" type="field"/>
      <column hidden="0" width="-1" name="RE1" type="field"/>
      <column hidden="0" width="-1" name="RE2" type="field"/>
      <column hidden="0" width="-1" name="RE3" type="field"/>
      <column hidden="0" width="-1" name="RE4" type="field"/>
      <column hidden="0" width="-1" name="RE5" type="field"/>
      <column hidden="0" width="-1" name="PERCENT" type="field"/>
      <column hidden="0" width="-1" name="PC1" type="field"/>
      <column hidden="0" width="-1" name="PC2" type="field"/>
      <column hidden="0" width="-1" name="PC3" type="field"/>
      <column hidden="0" width="-1" name="PC4" type="field"/>
      <column hidden="0" width="-1" name="PC5" type="field"/>
      <column hidden="0" width="-1" name="RE_LABEL" type="field"/>
      <column hidden="0" width="-1" name="PC_LABEL" type="field"/>
      <column hidden="0" width="-1" name="LANDZONE" type="field"/>
      <column hidden="0" width="-1" name="BD_STATUS" type="field"/>
      <column hidden="0" width="-1" name="BD_SYMBOL" type="field"/>
      <column hidden="0" width="-1" name="VM_CLASS" type="field"/>
      <column hidden="0" width="-1" name="VM_SYMBOL" type="field"/>
      <column hidden="0" width="-1" name="VM_POLY" type="field"/>
      <column hidden="0" width="-1" name="VERSION" type="field"/>
      <column hidden="0" width="-1" name="L" type="field"/>
      <column hidden="0" width="-1" name="V" type="field"/>
      <column hidden="0" width="-1" name="BVG1M" type="field"/>
      <column hidden="0" width="-1" name="BVG1M_PC" type="field"/>
      <column hidden="0" width="-1" name="DBVG1M" type="field"/>
      <column hidden="0" width="-1" name="DBVG2M" type="field"/>
      <column hidden="0" width="-1" name="DBVG5M" type="field"/>
      <column hidden="0" width="262" name="S20AH" type="field"/>
      <column hidden="0" width="-1" name="S20AI" type="field"/>
      <column hidden="0" width="-1" name="S20AM" type="field"/>
      <column hidden="0" width="-1" name="SOURCE" type="field"/>
      <column hidden="0" width="-1" name="VM_STATUS" type="field"/>
      <column hidden="0" width="-1" name="VM_STAT_ID" type="field"/>
      <column hidden="0" width="-1" name="SCALE" type="field"/>
      <column hidden="0" width="-1" name="OBJECTID" type="field"/>
      <column hidden="0" width="-1" name="MAP_NO" type="field"/>
      <column hidden="1" width="-1" type="actions"/>
      <column hidden="0" width="-1" name="layer" type="field"/>
      <column hidden="0" width="-1" name="path" type="field"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field name="BD_STATUS" editable="1"/>
    <field name="BD_SYMBOL" editable="1"/>
    <field name="BVG1M" editable="1"/>
    <field name="BVG1M_PC" editable="1"/>
    <field name="DBVG1M" editable="1"/>
    <field name="DBVG2M" editable="1"/>
    <field name="DBVG5M" editable="1"/>
    <field name="L" editable="1"/>
    <field name="LANDZONE" editable="1"/>
    <field name="MAP_NO" editable="1"/>
    <field name="OBJECTID" editable="1"/>
    <field name="PC1" editable="1"/>
    <field name="PC2" editable="1"/>
    <field name="PC3" editable="1"/>
    <field name="PC4" editable="1"/>
    <field name="PC5" editable="1"/>
    <field name="PC_LABEL" editable="1"/>
    <field name="PERCENT" editable="1"/>
    <field name="RE" editable="1"/>
    <field name="RE1" editable="1"/>
    <field name="RE2" editable="1"/>
    <field name="RE3" editable="1"/>
    <field name="RE4" editable="1"/>
    <field name="RE5" editable="1"/>
    <field name="RE_LABEL" editable="1"/>
    <field name="S20AH" editable="1"/>
    <field name="S20AI" editable="1"/>
    <field name="S20AM" editable="1"/>
    <field name="SCALE" editable="1"/>
    <field name="SOURCE" editable="1"/>
    <field name="V" editable="1"/>
    <field name="VERSION" editable="1"/>
    <field name="VM_CLASS" editable="1"/>
    <field name="VM_POLY" editable="1"/>
    <field name="VM_STATUS" editable="1"/>
    <field name="VM_STAT_ID" editable="1"/>
    <field name="VM_SYMBOL" editable="1"/>
    <field name="layer" editable="1"/>
    <field name="path" editable="1"/>
  </editable>
  <labelOnTop>
    <field name="BD_STATUS" labelOnTop="0"/>
    <field name="BD_SYMBOL" labelOnTop="0"/>
    <field name="BVG1M" labelOnTop="0"/>
    <field name="BVG1M_PC" labelOnTop="0"/>
    <field name="DBVG1M" labelOnTop="0"/>
    <field name="DBVG2M" labelOnTop="0"/>
    <field name="DBVG5M" labelOnTop="0"/>
    <field name="L" labelOnTop="0"/>
    <field name="LANDZONE" labelOnTop="0"/>
    <field name="MAP_NO" labelOnTop="0"/>
    <field name="OBJECTID" labelOnTop="0"/>
    <field name="PC1" labelOnTop="0"/>
    <field name="PC2" labelOnTop="0"/>
    <field name="PC3" labelOnTop="0"/>
    <field name="PC4" labelOnTop="0"/>
    <field name="PC5" labelOnTop="0"/>
    <field name="PC_LABEL" labelOnTop="0"/>
    <field name="PERCENT" labelOnTop="0"/>
    <field name="RE" labelOnTop="0"/>
    <field name="RE1" labelOnTop="0"/>
    <field name="RE2" labelOnTop="0"/>
    <field name="RE3" labelOnTop="0"/>
    <field name="RE4" labelOnTop="0"/>
    <field name="RE5" labelOnTop="0"/>
    <field name="RE_LABEL" labelOnTop="0"/>
    <field name="S20AH" labelOnTop="0"/>
    <field name="S20AI" labelOnTop="0"/>
    <field name="S20AM" labelOnTop="0"/>
    <field name="SCALE" labelOnTop="0"/>
    <field name="SOURCE" labelOnTop="0"/>
    <field name="V" labelOnTop="0"/>
    <field name="VERSION" labelOnTop="0"/>
    <field name="VM_CLASS" labelOnTop="0"/>
    <field name="VM_POLY" labelOnTop="0"/>
    <field name="VM_STATUS" labelOnTop="0"/>
    <field name="VM_STAT_ID" labelOnTop="0"/>
    <field name="VM_SYMBOL" labelOnTop="0"/>
    <field name="layer" labelOnTop="0"/>
    <field name="path" labelOnTop="0"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>VM_STAT_ID</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
