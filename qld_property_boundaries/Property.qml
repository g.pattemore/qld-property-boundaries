<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis minScale="1e+08" simplifyAlgorithm="0" hasScaleBasedVisibilityFlag="0" maxScale="0" styleCategories="AllStyleCategories" labelsEnabled="1" simplifyLocal="1" simplifyDrawingHints="1" simplifyDrawingTol="1" version="3.10.4-A Coruña" simplifyMaxScale="1" readOnly="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 forceraster="0" type="singleSymbol" symbollevels="0" enableorderby="0">
    <symbols>
      <symbol type="fill" alpha="1" name="0" force_rhr="0" clip_to_extent="1">
        <layer class="SimpleFill" locked="0" pass="0" enabled="1">
          <prop k="border_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="color" v="183,72,75,255"/>
          <prop k="joinstyle" v="bevel"/>
          <prop k="offset" v="0,0"/>
          <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
          <prop k="offset_unit" v="MM"/>
          <prop k="outline_color" v="35,35,35,255"/>
          <prop k="outline_style" v="solid"/>
          <prop k="outline_width" v="0.66"/>
          <prop k="outline_width_unit" v="MM"/>
          <prop k="style" v="no"/>
          <data_defined_properties>
            <Option type="Map">
              <Option value="" type="QString" name="name"/>
              <Option name="properties"/>
              <Option value="collection" type="QString" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style fontKerning="1" fontWeight="75" fontFamily="Ubuntu" namedStyle="Bold" fontSize="12" fontUnderline="0" fontCapitals="0" textColor="0,0,0,255" useSubstitutions="0" textOrientation="horizontal" textOpacity="1" isExpression="0" previewBkgrdColor="255,255,255,255" fontSizeUnit="Point" fontLetterSpacing="0" fieldName="LOTPLAN" fontStrikeout="0" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontItalic="0" fontWordSpacing="0" multilineHeight="1" blendMode="0">
        <text-buffer bufferSizeUnits="MM" bufferJoinStyle="128" bufferOpacity="1" bufferDraw="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferColor="255,255,255,255" bufferSize="1" bufferBlendMode="0" bufferNoFill="1"/>
        <background shapeOffsetMapUnitScale="3x:0,0,0,0,0,0" shapeRotationType="0" shapeSizeY="0" shapeOffsetY="0" shapeSizeX="0" shapeBorderColor="128,128,128,255" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeDraw="0" shapeRotation="0" shapeJoinStyle="64" shapeSVGFile="" shapeType="0" shapeBlendMode="0" shapeRadiiY="0" shapeRadiiUnit="MM" shapeBorderWidth="0" shapeBorderWidthUnit="MM" shapeOffsetX="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeOpacity="1" shapeSizeUnit="MM" shapeRadiiX="0" shapeSizeType="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetUnit="MM" shapeFillColor="255,255,255,255">
          <symbol type="marker" alpha="1" name="markerSymbol" force_rhr="0" clip_to_extent="1">
            <layer class="SimpleMarker" locked="0" pass="0" enabled="1">
              <prop k="angle" v="0"/>
              <prop k="color" v="141,90,153,255"/>
              <prop k="horizontal_anchor_point" v="1"/>
              <prop k="joinstyle" v="bevel"/>
              <prop k="name" v="circle"/>
              <prop k="offset" v="0,0"/>
              <prop k="offset_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="offset_unit" v="MM"/>
              <prop k="outline_color" v="35,35,35,255"/>
              <prop k="outline_style" v="solid"/>
              <prop k="outline_width" v="0"/>
              <prop k="outline_width_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="outline_width_unit" v="MM"/>
              <prop k="scale_method" v="diameter"/>
              <prop k="size" v="2"/>
              <prop k="size_map_unit_scale" v="3x:0,0,0,0,0,0"/>
              <prop k="size_unit" v="MM"/>
              <prop k="vertical_anchor_point" v="1"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option value="" type="QString" name="name"/>
                  <Option name="properties"/>
                  <Option value="collection" type="QString" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowOffsetUnit="MM" shadowRadius="1.5" shadowOffsetAngle="135" shadowOffsetDist="1" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowBlendMode="6" shadowRadiusUnit="MM" shadowUnder="0" shadowRadiusAlphaOnly="0" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowDraw="0" shadowOffsetGlobal="1" shadowColor="0,0,0,255" shadowOpacity="0.7" shadowScale="100"/>
        <dd_properties>
          <Option type="Map">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format rightDirectionSymbol=">" plussign="0" formatNumbers="0" autoWrapLength="0" placeDirectionSymbol="0" multilineAlign="0" leftDirectionSymbol="&lt;" reverseDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" addDirectionSymbol="0" decimals="3" wrapChar=""/>
      <placement distMapUnitScale="3x:0,0,0,0,0,0" rotationAngle="0" dist="0" preserveRotation="1" distUnits="MM" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" layerType="PolygonGeometry" priority="10" overrunDistanceUnit="MM" offsetUnits="MM" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" offsetType="0" fitInPolygonOnly="0" repeatDistanceUnits="MM" overrunDistance="0" geometryGeneratorType="PointGeometry" geometryGenerator="" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0" maxCurvedCharAngleIn="25" yOffset="0" placement="0" maxCurvedCharAngleOut="-25" placementFlags="10" xOffset="0" centroidWhole="0" repeatDistance="0" centroidInside="0" geometryGeneratorEnabled="0" quadOffset="4"/>
      <rendering mergeLines="0" scaleVisibility="0" obstacle="1" minFeatureSize="1" scaleMax="0" drawLabels="1" limitNumLabels="0" maxNumLabels="2000" upsidedownLabels="0" fontMaxPixelSize="10000" fontLimitPixelSize="0" obstacleFactor="2" zIndex="0" scaleMin="0" obstacleType="1" labelPerPart="1" fontMinPixelSize="3" displayAll="0"/>
      <dd_properties>
        <Option type="Map">
          <Option value="" type="QString" name="name"/>
          <Option name="properties"/>
          <Option value="collection" type="QString" name="type"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option value="pole_of_inaccessibility" type="QString" name="anchorPoint"/>
          <Option type="Map" name="ddProperties">
            <Option value="" type="QString" name="name"/>
            <Option name="properties"/>
            <Option value="collection" type="QString" name="type"/>
          </Option>
          <Option value="false" type="bool" name="drawToAllParts"/>
          <Option value="0" type="QString" name="enabled"/>
          <Option value="&lt;symbol type=&quot;line&quot; alpha=&quot;1&quot; name=&quot;symbol&quot; force_rhr=&quot;0&quot; clip_to_extent=&quot;1&quot;>&lt;layer class=&quot;SimpleLine&quot; locked=&quot;0&quot; pass=&quot;0&quot; enabled=&quot;1&quot;>&lt;prop k=&quot;capstyle&quot; v=&quot;square&quot;/>&lt;prop k=&quot;customdash&quot; v=&quot;5;2&quot;/>&lt;prop k=&quot;customdash_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;customdash_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;draw_inside_polygon&quot; v=&quot;0&quot;/>&lt;prop k=&quot;joinstyle&quot; v=&quot;bevel&quot;/>&lt;prop k=&quot;line_color&quot; v=&quot;60,60,60,255&quot;/>&lt;prop k=&quot;line_style&quot; v=&quot;solid&quot;/>&lt;prop k=&quot;line_width&quot; v=&quot;0.3&quot;/>&lt;prop k=&quot;line_width_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;offset&quot; v=&quot;0&quot;/>&lt;prop k=&quot;offset_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;prop k=&quot;offset_unit&quot; v=&quot;MM&quot;/>&lt;prop k=&quot;ring_filter&quot; v=&quot;0&quot;/>&lt;prop k=&quot;use_custom_dash&quot; v=&quot;0&quot;/>&lt;prop k=&quot;width_map_unit_scale&quot; v=&quot;3x:0,0,0,0,0,0&quot;/>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option value=&quot;&quot; type=&quot;QString&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option value=&quot;collection&quot; type=&quot;QString&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" type="QString" name="lineSymbol"/>
          <Option value="0" type="double" name="minLength"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="minLengthMapUnitScale"/>
          <Option value="MM" type="QString" name="minLengthUnit"/>
          <Option value="0" type="double" name="offsetFromAnchor"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromAnchorMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromAnchorUnit"/>
          <Option value="0" type="double" name="offsetFromLabel"/>
          <Option value="3x:0,0,0,0,0,0" type="QString" name="offsetFromLabelMapUnitScale"/>
          <Option value="MM" type="QString" name="offsetFromLabelUnit"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="variableNames"/>
    <property key="variableValues"/>
  </customproperties>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory lineSizeScale="3x:0,0,0,0,0,0" minScaleDenominator="0" opacity="1" scaleDependency="Area" penAlpha="255" enabled="0" penWidth="0" scaleBasedVisibility="0" penColor="#000000" height="15" rotationOffset="270" lineSizeType="MM" backgroundAlpha="255" maxScaleDenominator="1e+08" barWidth="5" labelPlacementMethod="XHeight" sizeType="MM" backgroundColor="#ffffff" diagramOrientation="Up" sizeScale="3x:0,0,0,0,0,0" width="15" minimumSize="0">
      <fontProperties description="Ubuntu,11,-1,5,50,0,0,0,0,0" style=""/>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings obstacle="0" showAll="1" placement="1" zIndex="0" dist="0" linePlacementFlags="18" priority="0">
    <properties>
      <Option type="Map">
        <Option value="" type="QString" name="name"/>
        <Option name="properties"/>
        <Option value="collection" type="QString" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration type="Map">
      <Option type="Map" name="QgsGeometryGapCheck">
        <Option value="0" type="double" name="allowedGapsBuffer"/>
        <Option value="false" type="bool" name="allowedGapsEnabled"/>
        <Option value="" type="QString" name="allowedGapsLayer"/>
      </Option>
    </checkConfiguration>
  </geometryOptions>
  <fieldConfiguration>
    <field name="OBJECTID">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="LOT">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="PLAN">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="LOTPLAN">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="TENURE">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="LOT_AREA">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="EXCL_AREA">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="LOT_VOLUME">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="FEAT_NAME">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ALIAS_NAME">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ACC_CODE">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="SURV_IND">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="COVER_TYP">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="PARCEL_TYP">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="LOCALITY">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="SHIRE_NAME">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="O_SHAPE">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="SMIS_MAP">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="OBJECTID" index="0" name=""/>
    <alias field="LOT" index="1" name=""/>
    <alias field="PLAN" index="2" name=""/>
    <alias field="LOTPLAN" index="3" name=""/>
    <alias field="TENURE" index="4" name=""/>
    <alias field="LOT_AREA" index="5" name=""/>
    <alias field="EXCL_AREA" index="6" name=""/>
    <alias field="LOT_VOLUME" index="7" name=""/>
    <alias field="FEAT_NAME" index="8" name=""/>
    <alias field="ALIAS_NAME" index="9" name=""/>
    <alias field="ACC_CODE" index="10" name=""/>
    <alias field="SURV_IND" index="11" name=""/>
    <alias field="COVER_TYP" index="12" name=""/>
    <alias field="PARCEL_TYP" index="13" name=""/>
    <alias field="LOCALITY" index="14" name=""/>
    <alias field="SHIRE_NAME" index="15" name=""/>
    <alias field="O_SHAPE" index="16" name=""/>
    <alias field="SMIS_MAP" index="17" name=""/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default field="OBJECTID" applyOnUpdate="0" expression=""/>
    <default field="LOT" applyOnUpdate="0" expression=""/>
    <default field="PLAN" applyOnUpdate="0" expression=""/>
    <default field="LOTPLAN" applyOnUpdate="0" expression=""/>
    <default field="TENURE" applyOnUpdate="0" expression=""/>
    <default field="LOT_AREA" applyOnUpdate="0" expression=""/>
    <default field="EXCL_AREA" applyOnUpdate="0" expression=""/>
    <default field="LOT_VOLUME" applyOnUpdate="0" expression=""/>
    <default field="FEAT_NAME" applyOnUpdate="0" expression=""/>
    <default field="ALIAS_NAME" applyOnUpdate="0" expression=""/>
    <default field="ACC_CODE" applyOnUpdate="0" expression=""/>
    <default field="SURV_IND" applyOnUpdate="0" expression=""/>
    <default field="COVER_TYP" applyOnUpdate="0" expression=""/>
    <default field="PARCEL_TYP" applyOnUpdate="0" expression=""/>
    <default field="LOCALITY" applyOnUpdate="0" expression=""/>
    <default field="SHIRE_NAME" applyOnUpdate="0" expression=""/>
    <default field="O_SHAPE" applyOnUpdate="0" expression=""/>
    <default field="SMIS_MAP" applyOnUpdate="0" expression=""/>
  </defaults>
  <constraints>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="OBJECTID" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="LOT" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="PLAN" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="LOTPLAN" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="TENURE" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="LOT_AREA" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="EXCL_AREA" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="LOT_VOLUME" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="FEAT_NAME" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="ALIAS_NAME" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="ACC_CODE" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="SURV_IND" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="COVER_TYP" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="PARCEL_TYP" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="LOCALITY" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="SHIRE_NAME" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="O_SHAPE" exp_strength="0"/>
    <constraint constraints="0" notnull_strength="0" unique_strength="0" field="SMIS_MAP" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint exp="" field="OBJECTID" desc=""/>
    <constraint exp="" field="LOT" desc=""/>
    <constraint exp="" field="PLAN" desc=""/>
    <constraint exp="" field="LOTPLAN" desc=""/>
    <constraint exp="" field="TENURE" desc=""/>
    <constraint exp="" field="LOT_AREA" desc=""/>
    <constraint exp="" field="EXCL_AREA" desc=""/>
    <constraint exp="" field="LOT_VOLUME" desc=""/>
    <constraint exp="" field="FEAT_NAME" desc=""/>
    <constraint exp="" field="ALIAS_NAME" desc=""/>
    <constraint exp="" field="ACC_CODE" desc=""/>
    <constraint exp="" field="SURV_IND" desc=""/>
    <constraint exp="" field="COVER_TYP" desc=""/>
    <constraint exp="" field="PARCEL_TYP" desc=""/>
    <constraint exp="" field="LOCALITY" desc=""/>
    <constraint exp="" field="SHIRE_NAME" desc=""/>
    <constraint exp="" field="O_SHAPE" desc=""/>
    <constraint exp="" field="SMIS_MAP" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction key="Canvas" value="{00000000-0000-0000-0000-000000000000}"/>
  </attributeactions>
  <attributetableconfig actionWidgetStyle="dropDown" sortExpression="" sortOrder="0">
    <columns>
      <column hidden="0" type="field" name="OBJECTID" width="-1"/>
      <column hidden="0" type="field" name="LOT" width="-1"/>
      <column hidden="0" type="field" name="PLAN" width="-1"/>
      <column hidden="0" type="field" name="LOTPLAN" width="-1"/>
      <column hidden="0" type="field" name="TENURE" width="-1"/>
      <column hidden="0" type="field" name="LOT_AREA" width="-1"/>
      <column hidden="0" type="field" name="EXCL_AREA" width="-1"/>
      <column hidden="0" type="field" name="LOT_VOLUME" width="-1"/>
      <column hidden="0" type="field" name="FEAT_NAME" width="-1"/>
      <column hidden="0" type="field" name="ALIAS_NAME" width="-1"/>
      <column hidden="0" type="field" name="ACC_CODE" width="-1"/>
      <column hidden="0" type="field" name="SURV_IND" width="-1"/>
      <column hidden="0" type="field" name="COVER_TYP" width="-1"/>
      <column hidden="0" type="field" name="PARCEL_TYP" width="-1"/>
      <column hidden="0" type="field" name="LOCALITY" width="-1"/>
      <column hidden="0" type="field" name="SHIRE_NAME" width="-1"/>
      <column hidden="0" type="field" name="O_SHAPE" width="-1"/>
      <column hidden="0" type="field" name="SMIS_MAP" width="-1"/>
      <column hidden="1" type="actions" width="-1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
QGIS forms can have a Python function that is called when the form is
opened.

Use this function to add extra logic to your forms.

Enter the name of the function in the "Python Init function"
field.
An example follows:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
	geom = feature.geometry()
	control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="ACC_CODE"/>
    <field editable="1" name="ALIAS_NAME"/>
    <field editable="1" name="COVER_TYP"/>
    <field editable="1" name="EXCL_AREA"/>
    <field editable="1" name="FEAT_NAME"/>
    <field editable="1" name="LOCALITY"/>
    <field editable="1" name="LOT"/>
    <field editable="1" name="LOTPLAN"/>
    <field editable="1" name="LOT_AREA"/>
    <field editable="1" name="LOT_VOLUME"/>
    <field editable="1" name="OBJECTID"/>
    <field editable="1" name="O_SHAPE"/>
    <field editable="1" name="PARCEL_TYP"/>
    <field editable="1" name="PLAN"/>
    <field editable="1" name="SHIRE_NAME"/>
    <field editable="1" name="SMIS_MAP"/>
    <field editable="1" name="SURV_IND"/>
    <field editable="1" name="TENURE"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="ACC_CODE"/>
    <field labelOnTop="0" name="ALIAS_NAME"/>
    <field labelOnTop="0" name="COVER_TYP"/>
    <field labelOnTop="0" name="EXCL_AREA"/>
    <field labelOnTop="0" name="FEAT_NAME"/>
    <field labelOnTop="0" name="LOCALITY"/>
    <field labelOnTop="0" name="LOT"/>
    <field labelOnTop="0" name="LOTPLAN"/>
    <field labelOnTop="0" name="LOT_AREA"/>
    <field labelOnTop="0" name="LOT_VOLUME"/>
    <field labelOnTop="0" name="OBJECTID"/>
    <field labelOnTop="0" name="O_SHAPE"/>
    <field labelOnTop="0" name="PARCEL_TYP"/>
    <field labelOnTop="0" name="PLAN"/>
    <field labelOnTop="0" name="SHIRE_NAME"/>
    <field labelOnTop="0" name="SMIS_MAP"/>
    <field labelOnTop="0" name="SURV_IND"/>
    <field labelOnTop="0" name="TENURE"/>
  </labelOnTop>
  <widgets/>
  <previewExpression>FEAT_NAME</previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
