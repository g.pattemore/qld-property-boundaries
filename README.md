# Queensland property boundaries

Loads Queensland property boundaries from lot/plan numbers

## Installing
[Download .ZIP](https://gitlab.com/RuralPear/qld-property-boundaries/-/raw/master/qld_property_boundaries.zip?inline=false "Download .ZIP")